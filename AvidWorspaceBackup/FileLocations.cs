﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvidWorkspaceResync
{
    class FileLocations
    {
        public string Path { get; set; }
        public int FileName { get; set; }
    }
}
