﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace AvidWorkspaceResync
{
    internal class Logger
    {
        private string _logPath = "";
        private static ReaderWriterLockSlim _readWriteLock = new ReaderWriterLockSlim();

        public Logger(string LogPath)
        {
            _logPath = LogPath;
            if (File.Exists(LogPath))
            {
                FileInfo f = new FileInfo(LogPath);
                if (f.Length > 5 * 1024 * 1024)
                {
                    string ArchiveName = LogPath + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".log";
                    File.Delete(ArchiveName); // Delete the existing file if exists
                    File.Move(LogPath, ArchiveName);
                }
            }
        }

        public void WriteLog(string Message)
        {
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_logPath));
            try
            {
                using (StreamWriter w = File.AppendText(_logPath))
                {
                    w.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + Message);
                    w.Close();
                }
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }

        public void WriteConsoleAndLog(string Message)
        {
            _readWriteLock.EnterWriteLock();        //enable lock
            Directory.CreateDirectory(Path.GetDirectoryName(_logPath));
            try
            {
                using (StreamWriter w = File.AppendText(_logPath))
                {
                    w.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + Message);
                    w.Close();
                    Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + Message);
                }
            }
            finally
            {
                _readWriteLock.ExitWriteLock();         //release lock
            }
        }
    }
}