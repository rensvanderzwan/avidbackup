﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentFTP;  // See https://github.com/robinrodricks/
using System.IO;
using System.Configuration;
using System.Threading;

namespace AvidWorkspaceResync
{
    class BackupNexis
    {
        // Raw SMB and FTP File object
        private List<FileInfo> _smbList = new List<FileInfo>();
        private List<FtpListItem> _ftpList = new List<FtpListItem>();

        // Common SMB/FTP File object 
        private List<CommonBackupFileInfo> _commonSMBList = new List<CommonBackupFileInfo>();
        private List<CommonBackupFileInfo> _commonFTPList = new List<CommonBackupFileInfo>();

        private string _ftpServer = ConfigurationManager.AppSettings["FTPServer"];
        private string _ftpLogin = ConfigurationManager.AppSettings["FTPLogin"];
        private string _ftpPassW = ConfigurationManager.AppSettings["FTPPassW"];
        private string _ftpWorkDir = ConfigurationManager.AppSettings["FTPWorkDir"];
        private string _dataConnectionType = ConfigurationManager.AppSettings["DataConnectionType"];
        private FtpClient _ftpClient;

        private string _sourceDir = ConfigurationManager.AppSettings["SourcePath"];
        private string _server = ConfigurationManager.AppSettings["Server"];
        private string _share = ConfigurationManager.AppSettings["Share"];
        private string _login = ConfigurationManager.AppSettings["Login"];
        private string _password = ConfigurationManager.AppSettings["Password"];
        private Connect _smbClient;
        private Logger _logger = new Logger(ConfigurationManager.AppSettings["logPath"]);

        static List<string> _excludedDirectories = ConfigurationManager.AppSettings["ExclusionDirectories"].Split(';').ToList();


        public BackupNexis()
        {
            _logger.WriteConsoleAndLog("Info: ################ Start ################");

            _smbClient = ConnectToSMBServer(_server,_share, _login, _password);
            if ( _smbClient != null)
            {
                // check out https://stackoverflow.com/questions/295538/how-to-provide-user-name-and-password-when-connecting-to-a-network-share
                DirectoryInfo dirInfo = new DirectoryInfo($"\\\\{_server}\\{_share}{_sourceDir}");
                _smbList = WalkArgyfDirectoryTree(dirInfo);
                _logger.WriteConsoleAndLog($"Info: Found {_smbList.Count()} files on Nexis");
            }
            else
            {
                _logger.WriteConsoleAndLog($"Error: Not Connected to ALTO, Exit");
                Environment.Exit(-1);
            }


            _ftpClient = ConnectToFTPServer(_ftpServer, _ftpLogin, _ftpPassW, _ftpWorkDir);
            if (_ftpClient.IsConnected)
            {
                _ftpList = getRecursiveList(_ftpClient, _ftpWorkDir, new List<FtpListItem>());
                _logger.WriteConsoleAndLog($"Info: Found {_ftpList.Count()} files on ALTO");
            }
            else
            {
                _logger.WriteConsoleAndLog($"Error: Not Connected to FTP, Exit");
                Environment.Exit(-1);
            }

            ConvertSMBFTPtoCommonList(_ftpList,_smbList);

            // to Backup
            List<CommonBackupFileInfo> mustBackup = _commonSMBList.Except(_commonFTPList, new FileInfoObjectEqualityComparer()).ToList();
            // to delete
            List<CommonBackupFileInfo> mustDelete = _commonFTPList.Except(_commonSMBList, new FileInfoObjectEqualityComparer()).ToList();

            _logger.WriteConsoleAndLog($"{mustBackup.Count()} files to Backup");

            foreach(CommonBackupFileInfo item in mustBackup)
            {
                doUpload(_ftpClient, item);
            }

            DisconnectFromFTPServer(_ftpClient);
            DisconnectFromSMBServer(_server, _share);


            _logger.WriteConsoleAndLog("Info: ################ End ################");
        }

        private void ConvertSMBFTPtoCommonList(List<FtpListItem> FTPList, List<FileInfo> SMBList)
        {

            foreach(FtpListItem ftpItem in FTPList)
            {
                CommonBackupFileInfo cfi = new CommonBackupFileInfo();
                cfi.FullName = ftpItem.FullName;
                cfi.CompareName = ftpItem.FullName;
                cfi.Size = ftpItem.Size;
                cfi.CreationDate = ftpItem.Created;
                cfi.ModifiedDate = ftpItem.Modified;
                _commonFTPList.Add(cfi);
            }

            foreach(FileInfo smbItem in SMBList)
            {
                CommonBackupFileInfo cfi = new CommonBackupFileInfo();
                cfi.FullName = smbItem.FullName;
                cfi.CompareName = smbItem.FullName.Replace($"\\\\{_server}", "").Replace("\\", "/");
                cfi.Size = smbItem.Length;
                cfi.CreationDate = smbItem.CreationTime;
                cfi.ModifiedDate = smbItem.LastWriteTime;
                _commonSMBList.Add(cfi);
            }
        }


        public class FileInfoObjectEqualityComparer : IEqualityComparer<CommonBackupFileInfo>
        {

            #region IEqualityComparer<ThisClass> Members


            public bool Equals(CommonBackupFileInfo x, CommonBackupFileInfo y)
            {
                //no null check here, you might want to do that, or correct that to compare just one part of your object
                return x.CompareName == y.CompareName && x.Size == y.Size;
            }


            public int GetHashCode(CommonBackupFileInfo obj)
            {
                unchecked
                {
                    var hash = 17;
                    //same here, if you only want to get a hashcode on a, remove the line with b
                    hash = hash * 23 + obj.CompareName.GetHashCode();
                    hash = hash * 23 + obj.Size.GetHashCode();

                    return hash;
                }
            }

            #endregion
        }


        private List<FtpListItem> getRecursiveList(FtpClient ftpClient, string workdir, List<FtpListItem> ItemList)
        {
            FtpListItem[] list = getList(ftpClient);
            _logger.WriteConsoleAndLog($"Info: /{workdir}/ #files: {list.Where(x => x.Type == FtpFileSystemObjectType.File).Count() }");
            foreach (FtpListItem item in list)
            {
                switch (item.Type)
                {
                    case FtpFileSystemObjectType.Directory:
                        ftpClient.SetWorkingDirectory("/" + workdir + "/" + item.Name);
                        List<FtpListItem> subList = getRecursiveList(ftpClient, workdir + "/" + item.Name, ItemList);
                        break;
                    case FtpFileSystemObjectType.File:
                        ItemList.Add(item);
                        break;
                    case FtpFileSystemObjectType.Link:
                        break;
                }
                //debug
                //if (ItemList.Count() > 10) return ItemList;
            }
            return ItemList;
        }

        private FtpListItem[] getList(FtpClient ftpClient)
        {
            FtpListItem[] list = ftpClient.GetListing(ftpClient.GetWorkingDirectory());
            return list;
        }





        private Connect ConnectToSMBServer(string Server, string Share, string Login, string Password)
        {
            var Connector = new Connect();
            try
            {
              //  DisconnectFromSMBServer(Server, Share);
                Connector.LoginToShare(Server, Share, Login, Password);
                _logger.WriteConsoleAndLog($"Info: Succesfully Connected to SMB server: \\\\{Server}\\{Share}\\");
                return Connector;
            }
            catch ( Exception ex)
            {
                _logger.WriteConsoleAndLog($"Error: Unable to connected to {Server}\\{Share} reason: {ex.Message}");
                return null;
            }


        }

        private void DisconnectFromSMBServer(string Server, string Share)
        {
            if (_smbClient != null)
                _smbClient.LogoutFromShare(Server, Share);
        }

        private void doUpload(FtpClient FTPClient, CommonBackupFileInfo FleTodo)
        {
            Progress<double> progress = new Progress<double>(x =>
            {
                Console.Write("\r{0:N1}% ", Math.Round(x,1));
            });

                bool TransferResult = false;
                try
                {
                    Console.Write("\n       " + FleTodo.FullName);
                    var watch = System.Diagnostics.Stopwatch.StartNew();
                    TransferResult = FTPClient.UploadFile($"{FleTodo.FullName}", FleTodo.CompareName, FtpExists.Overwrite, true, FtpVerify.Delete, progress);
                    watch.Stop();
                    var elapsedS = (int)(watch.ElapsedMilliseconds / 1000);
                    Console.Write($"{FleTodo.FullName.PadRight(132, ' ')} time:{elapsedS.ToString().PadLeft(4, ' ')} sec");
                    _logger.WriteLog($"Info: Succes Transfer of File: {FleTodo.FullName}, {elapsedS} sec\t, succes: {TransferResult.ToString()}");
            }
                catch ( FtpException ex)
                {
                    _logger.WriteConsoleAndLog($"Error: Failure during FTP transfer of file {FleTodo.FullName} errormessage: {ex.Message} innerexeption: {ex.InnerException}");
                    Environment.Exit(-1);
                }
                catch ( Exception ex)
                {
                    _logger.WriteConsoleAndLog($"Error: Failure during FTP transfer of file {FleTodo.FullName} errormessage: {ex.Message} innerexeption: {ex.InnerException}");
                    Environment.Exit(-1);
                }
                HandleConsoleQuitKey();
        }


        public List<FileInfo> WalkArgyfDirectoryTree(System.IO.DirectoryInfo root)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;
            List<FileInfo> FileList = new List<FileInfo>();


            // First, process all the files directly under this folder
            try
            {
                files = root.GetFiles("*.*",SearchOption.TopDirectoryOnly).Where(s => s.Name.ToLower().EndsWith(".mxf")).ToArray();
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException ex)
            {
                // This code just writes out the message and continues to recurse.
                // You may decide to do something different here. For example, you
                // can try to elevate your privileges and access the file again.
                _logger.WriteConsoleAndLog($"Error: UnauthorizedAccessException: {ex.Message}");
            }
            catch (System.IO.DirectoryNotFoundException ex)
            {
                _logger.WriteConsoleAndLog($"Error: DirectoryNotFoundException: {ex.Message}");
            }
            catch (Exception ex)
            {
                _logger.WriteConsoleAndLog($"Error: FileException: {ex.Message} InnerExeption: {ex.InnerException}");
            }

            _logger.WriteConsoleAndLog($"Info: {root.FullName}\\ #files: {files.Count()}");

            if (files != null)
            {
                FileList.AddRange(files);

                // Now find all the subdirectories under this directory.
                try
                {
                    subDirs = root.GetDirectories().Where(d => !isExcluded(_excludedDirectories, d.FullName)).ToArray();
                }
                catch(Exception ex)
                {
                    _logger.WriteConsoleAndLog($"Error: DirException: {ex.Message}");
                }
                


                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    FileList.AddRange(WalkArgyfDirectoryTree(dirInfo));
                    // debug
                    //if (FileList.Count() > 1000) return FileList;
                }
            }
            return FileList;
        }


        static bool isExcluded(List<string> exludedDirList, string target)
        {
            return exludedDirList.Any(d => new DirectoryInfo(target).Name.Equals(d));
        }


        private FtpClient ConnectToFTPServer(string FTPServer, string Login, string Password, string BaseDir)
        {
            FtpClient client = new FtpClient(FTPServer);
            client.Credentials = new System.Net.NetworkCredential(Login, Password);

            client.RetryAttempts = 1;
            client.ReadTimeout = 120000;
            client.ConnectTimeout = 120000;
            if ( _dataConnectionType == "AutoActive")
                client.DataConnectionType = FtpDataConnectionType.AutoActive;
            else if (_dataConnectionType == "AutoPassive")
                client.DataConnectionType = FtpDataConnectionType.AutoPassive;
            else
                client.DataConnectionType = FtpDataConnectionType.AutoPassive;
            client.TransferChunkSize = 524288; // 512kB
            _logger.WriteConsoleAndLog($"Info: Set Dataconnectiontype: {FtpDataConnectionType.AutoPassive.ToString()}");
            _logger.WriteConsoleAndLog($"Info: Set TransferChunkSize: {client.TransferChunkSize.ToString()}");

            try
            {
                client.Connect();
                client.SetWorkingDirectory(BaseDir);
                _logger.WriteConsoleAndLog($"Info: Succesfully connected to to FtpServer {FTPServer}");
                return client;
            }
            catch (Exception ex)
            {
                _logger.WriteConsoleAndLog($"Error: Unable to connect to FtpServer {FTPServer} {ex.Message}");
            }
            return client;
        }

        private void DisconnectFromFTPServer(FtpClient client)
        {
            if (client != null)
                client.Disconnect();
        }

        private void HandleConsoleQuitKey()
        {
            if (Console.KeyAvailable)
            {
                ConsoleKeyInfo cki = Console.ReadKey(true);
                if (cki.Key.ToString().ToUpper() == "Q") Console.WriteLine("\n\nquit key detected");
                Thread.Sleep(5000);
                _logger.WriteConsoleAndLog("Info: Program quit by User.");
                System.Environment.Exit(1);
            }
        }



        private void DoHashCheck(FtpClient FTPClient, string filePath)
        {
            // Doe een hashcheck tussen bron en destination file
            // Zie https://github.com/robinrodricks/FluentFTP/issues/92

            if (FTPClient.HashAlgorithms != FtpHashAlgorithm.NONE)
            {
                FtpHash hash;
                hash = FTPClient.GetHash(filePath);
                if (hash.Verify(filePath))
                {
                    Console.WriteLine("The computed hashes match!");
                }
                else
                {
                    Console.WriteLine("Ohh dear, the computed hashes don't match!");
                }
            }
        }
    }
}
