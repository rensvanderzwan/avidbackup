﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvidWorkspaceResync
{
    class CommonBackupFileInfo
    {
        public string FullName {get; set;}
        public String CompareName { get; set; }
        public long  Size { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
