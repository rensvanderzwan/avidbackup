﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using FluentFTP;  // See https://github.com/robinrodricks/

namespace NewFileIterator
{
    class FileCollector
    {
 
        private List<FileInfo> _smbList = new List<FileInfo>();
        private List<FtpListItem> _ftpList = new List<FtpListItem>();

        // Common SMB/FTP File object 
        private List<CommonFileObject> _commonSMBList = new List<CommonFileObject>();
        private List<CommonFileObject> _commonFTPList = new List<CommonFileObject>();


        private string _ftpServer = "192.168.17.123";
        private string _ftpLogin = "AVID_BACKUP";
        private string _ftpPassW = "avid1107";
        private string _ftpWorkDir = "Argyf/Avid MediaFiles/MXF/iNos-Main.18.19/";
        private string _dataConnectionType = "AutoPassive";
        private FtpClient _ftpClient;



        private List<string> _smbFileNameList = new List<string>();
        static List<string> _exclusionDirectories = "Creating;Poep".Split(';').ToList();
        static List<string> _inclusionExtensions = ".mxf".Split(';').ToList();
        private List<string> _excludedDirectories = new List<string>() { "Creating", "Temp", "Quarantined Files" };

        public FileCollector()
        {
            GetAllFiles();
        }

        public void GetAllFiles()
        {
            string path = @"\\OFAAVSD\Argyf\Avid MediaFiles\MXF\iNos-Main.18.19\";
            DirectoryInfo dirInfo = new DirectoryInfo(path);
            _smbList = WalkArgyfDirectoryTree(dirInfo);

            _ftpClient = ConnectToFTPServer(_ftpServer, _ftpLogin, _ftpPassW, _ftpWorkDir);
            _ftpList = getRecursiveList(_ftpClient, _ftpWorkDir, new List<FtpListItem>());

            ConvertSMBFTPtoCommonList(_ftpList, _smbList);

            // to Backup
            List<CommonFileObject> mustBackup = _commonSMBList.Except(_commonFTPList, new FileInfoObjectEqualityComparer()).ToList();
            // to delete
            List<CommonFileObject> mustDelete = _commonFTPList.Except(_commonSMBList, new FileInfoObjectEqualityComparer()).ToList();

        }

        private void ConvertSMBFTPtoCommonList(List<FtpListItem> FTPList, List<FileInfo> SMBList)
        {

            foreach (FtpListItem ftpItem in FTPList)
            {
                CommonFileObject cfi = new CommonFileObject();
                cfi.FullName = ftpItem.FullName;
                cfi.CompareName = ftpItem.FullName;
                cfi.Size = ftpItem.Size;
                cfi.CreationDate = ftpItem.Created;
                cfi.ModifiedDate = ftpItem.Modified;
                _commonFTPList.Add(cfi);
            }

            foreach (FileInfo smbItem in SMBList)
            {
                CommonFileObject cfi = new CommonFileObject();
                cfi.FullName = smbItem.FullName;
                cfi.CompareName = smbItem.FullName.Replace($"\\\\OFAAVSD", "").Replace("\\", "/");
                cfi.Size = smbItem.Length;
                cfi.CreationDate = smbItem.CreationTime;
                cfi.ModifiedDate = smbItem.LastWriteTime;
                _commonSMBList.Add(cfi);
            }
        }


        public class FileInfoObjectEqualityComparer : IEqualityComparer<CommonFileObject>
        {

            #region IEqualityComparer<ThisClass> Members


            public bool Equals(CommonFileObject x, CommonFileObject y)
            {
                //no null check here, you might want to do that, or correct that to compare just one part of your object
                return x.CompareName == y.CompareName && x.Size == y.Size;
            }


            public int GetHashCode(CommonFileObject obj)
            {
                unchecked
                {
                    var hash = 17;
                    //same here, if you only want to get a hashcode on a, remove the line with b
                    hash = hash * 23 + obj.CompareName.GetHashCode();
                    hash = hash * 23 + obj.Size.GetHashCode();

                    return hash;
                }
            }

            #endregion
        }


        private FtpClient ConnectToFTPServer(string FTPServer, string Login, string Password, string BaseDir)
        {
            FtpClient client = new FtpClient(FTPServer);
            client.Credentials = new System.Net.NetworkCredential(Login, Password);

            client.RetryAttempts = 1;
            client.ReadTimeout = 120000;
            client.ConnectTimeout = 120000;
            if (_dataConnectionType == "AutoActive")
                client.DataConnectionType = FtpDataConnectionType.AutoActive;
            else if (_dataConnectionType == "AutoPassive")
                client.DataConnectionType = FtpDataConnectionType.AutoPassive;
            else
                client.DataConnectionType = FtpDataConnectionType.AutoPassive;
            client.TransferChunkSize = 524288; // 512kB

            try
            {
                client.Connect();
                client.SetWorkingDirectory(BaseDir);
 
                return client;
            }
            catch (Exception ex)
            {
            }
            return client;
        }

        private List<FtpListItem> getRecursiveList(FtpClient ftpClient, string workdir, List<FtpListItem> ItemList)
        {
            FtpListItem[] list = getList(ftpClient);
           // _logger.WriteConsoleAndLog($"Info: /{workdir}/ #files: {list.Where(x => x.Type == FtpFileSystemObjectType.File).Count() }");
            foreach (FtpListItem item in list)
            {
                switch (item.Type)
                {
                    case FtpFileSystemObjectType.Directory:
                        ftpClient.SetWorkingDirectory("/" + workdir + "/" + item.Name);
                        List<FtpListItem> subList = getRecursiveList(ftpClient, workdir + "/" + item.Name, ItemList);
                        break;
                    case FtpFileSystemObjectType.File:
                        ItemList.Add(item);
                        break;
                    case FtpFileSystemObjectType.Link:
                        break;
                }
                //debug
                //if (ItemList.Count() > 10) return ItemList;
            }
            return ItemList;
        }



        private FtpListItem[] getList(FtpClient ftpClient)
        {
            FtpListItem[] list = ftpClient.GetListing(ftpClient.GetWorkingDirectory());
            return list;
        }

        public List<FileInfo> WalkArgyfDirectoryTree(System.IO.DirectoryInfo root)
        {
            System.IO.FileInfo[] files = null;
            System.IO.DirectoryInfo[] subDirs = null;
            List<FileInfo> FileList = new List<FileInfo>();


            // First, process all the files directly under this folder
              try
            {
                files = root.GetFiles("*.*").Where(s => s.Name.ToLower().EndsWith(".mxf")).ToArray();
            }
            // This is thrown if even one of the files requires permissions greater
            // than the application provides.
            catch (UnauthorizedAccessException e)
            {
                // This code just writes out the message and continues to recurse.
                // You may decide to do something different here. For example, you
                // can try to elevate your privileges and access the file again.
                Console.WriteLine(e.Message);
            }
            catch (System.IO.DirectoryNotFoundException e)
            {
                Console.WriteLine(e.Message);
            }

            Console.WriteLine($"{root.FullName} {files.Count()}");

            if (files != null)
            {
                //foreach (System.IO.FileInfo fi in files)
                //{
                //    FileList.Add(fi);
                //    // In this example, we only access the existing FileInfo object. If we
                //    // want to open, delete or modify the file, then
                //    // a try-catch block is required here to handle the case
                //    // where the file has been deleted since the call to TraverseTree().
                //}
                FileList.AddRange(files.ToArray());
                // Now find all the subdirectories under this directory.
                subDirs = root.GetDirectories().Where(d => !isExcluded(_excludedDirectories, d.FullName)).ToArray();


                foreach (System.IO.DirectoryInfo dirInfo in subDirs)
                {
                    // Resursive call for each subdirectory.
                    FileList.AddRange(WalkArgyfDirectoryTree(dirInfo));
                    // debug
                    if (FileList.Count() > 1000) return FileList;
                }
            }
            return FileList;
        }


        static bool isExcluded(List<string> exludedDirList, string target)
        {
            return exludedDirList.Any(d => new DirectoryInfo(target).Name.Equals(d));
        }

    }
}
